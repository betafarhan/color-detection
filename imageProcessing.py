import sys
import cv2
# import tensorflow as tf
# import keras as ks
from PyQt5.QtWidgets import QApplication, QDialog
from PyQt5.QtGui import *
from PyQt5.uic import loadUi
from PyQt5.QtCore import *
import numpy as np
import math

class imageProcessing(QDialog):
    def __init__(self):
        super(imageProcessing, self).__init__()
        loadUi('imageprocessing.ui', self)
        self.image = None
        self.start_webcam()

        self.Track.setCheckable(True)
        self.Track.toggled.connect(self.track_webcam_color)
        self.Track_Enabled = False

        self.Track_Color.clicked.connect(self.setcolorbola)
        #self.Track.clicked.connect(self.track_colored_object)

    def track_webcam_color(self, status):
        if status:
            self.Track_Enabled = True
            self.Track.setText('Stop Tracking')
        else:
            self.Track_Enabled = False
            self.Track.setText('Track Color')
        # self.Track.setText(str(status))

    def setcolorbola(self):
        self.colorbola_lower = np.array([self.Hmin.value(), self.Smin.value(), self.Vmin.value()], np.uint8)
        self.colorbola_upper = np.array([self.Hmax.value(), self.Smax.value(), self.Vmax.value()], np.uint8)
        self.erodebola = self.Erode.value()
        self.dilatebola = self.Dilate.value()

        self.HSV_Bola.setText('Min : ' + str(self.colorbola_lower) + 'Max : ' + str(self.colorbola_upper))

    def start_webcam(self):
        self.capture = cv2.VideoCapture(0)
        self.capture.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
        self.capture.set(cv2.CAP_PROP_FRAME_WIDTH, 640)

        self.timer = QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(5)

    def update_frame(self):
        ret, self.image = self.capture.read()
        self.image = cv2.flip(self.image, 1)
        # self.image = cv2.medianBlur(self.image, self.BlurV.value() + (self.BlurV.value() - 1))
        self.image = cv2.bilateralFilter(self.image, 5, 175, 175)
        self.displayimage(self.image, 1)

        hsv = cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV)

        self.colorbola_lower = np.array([self.Hmin.value(), self.Smin.value(), self.Vmin.value()], np.uint8)
        self.colorbola_upper = np.array([self.Hmax.value(), self.Smax.value(), self.Vmax.value()], np.uint8)

        color_mask = cv2.inRange(hsv, self.colorbola_lower, self.colorbola_upper)

        erode = cv2.erode(color_mask, None, iterations=self.Erode.value())
        dilate = cv2.dilate(erode, None, iterations=self.Dilate.value())

        # self.displayimage(color_mask, 2)
        self.displayimage(dilate, 2)

        if (self.Track_Enabled and self.Track_Color.isChecked):
            trackedimage = self.track_colored_object(self.image.copy())
            self.displayimage(trackedimage, 1)
        else:
            self.displayimage(self.image, 1)


    def track_colored_object(self, img):
        #self.katatrack.setText('dasdsadsa')
        # blur = cv2.blur(img,(3,3))
        # hsv= cv2.cvtColor(blur,cv2.COLOR_BGR2HSV)
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

        if self.Track_Color_check.isChecked():

            color_mask = cv2.inRange(hsv, self.colorbola_lower, self.colorbola_upper)

            # erode=cv2.erode(color_mask,None,iterations=2)
            # dilate=cv2.dilate(erode,None,iterations=10)

            erode = cv2.erode(color_mask, None, iterations=self.erodebola)
            dilate = cv2.dilate(erode, None, iterations=self.dilatebola)

            kernelopen = np.ones((5, 5))
            kernelclose = np.ones((20, 20))

            maskopen = cv2.morphologyEx(dilate, cv2.MORPH_OPEN, kernelopen)
            maskclose = cv2.morphologyEx(maskopen, cv2.MORPH_CLOSE, kernelclose)

            # cnts = (_, contours, hierarchy) = cv2.findContours(maskclose, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            contours, hierarchy = cv2.findContours(maskclose, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            # center = None

            for cnts in contours:
                area = cv2.contourArea(cnts)
                ''' M = cv2.moments(area)
                a = int(M["m10"]/M["m00"])
                b = int(M["m01"] / M["m00"])
                center = (a, b) '''

                if area > 5000:
                    ((x, y), radius) = cv2.minEnclosingCircle(cnts)

                    cv2.circle(img, (int(x), int(y)), int(radius), (0, 140, 255), 2)
                    # panjanggaris = math.pow(math.pow(180 - int(x), 2) + math.pow(150 - int(y), 2), 1 / 2.0)
                    panjang_x = math.pow(math.pow(180 - int(x), 2) + math.pow(150 - 150, 2), 1 / 2.0)
                    panjang_y = math.pow(math.pow(180 - 180, 2) + math.pow(150 - int(y), 2), 1 / 2.0)

                    # if (int(y)) <= 150:
                    #     if (int(x)) >= 180:
                    #         posisi = self.sudut(panjang_x, panjang_y)
                    #     else:
                    #         posisi = 0-self.sudut(panjang_x, panjang_y)
                    # else:
                    #     if (int(x)) >= 180:
                    #         posisi = 180-(self.sudut(panjang_x, panjang_y))
                    #     else:
                    #         posisi = 0-180+(self.sudut(panjang_x, panjang_y))

                    '''cv2.putText(img, "BOLA " + str(int(x)) + "," + str(int(y)) + "," + str(posisi),
                                (int(x - radius), int(y - radius)), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 140, 255, 2), 2)'''
                    cv2.putText(img, "OBJ 1 " + str((x,y)),
                                (int(x - radius), int(y - radius)), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 140, 255, 2), 2)

                    # draw a red line
                    cv2.line(img, (180, 150), (int(x), int(y)), (255, 0, 0), 4)
                    print(int(x - radius), int(y - radius), area)
        return img

    def displayimage(self, img, video=1):
        qformat = QImage.Format_Indexed8
        if len(img.shape) == 3:
            if img.shape[2] == 4:
                qformat = QImage.Format_RGBA8888
            else:
                qformat = QImage.Format_RGB888
        outimage = QImage(img, img.shape[1], img.shape[0], img.strides[0], qformat)
        outimage = outimage.rgbSwapped()

        if video == 1:
            self.frame_cam.setPixmap(QPixmap.fromImage(outimage))
            self.frame_cam.setScaledContents(True)
        if video == 2:
            self.frame_cam_2.setPixmap(QPixmap.fromImage(outimage))
            self.frame_cam_2.setScaledContents(True)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = imageProcessing()
    window.setWindowTitle('AAA')
    window.show()
    sys.exit(app.exec_())

# sock.close()
